﻿$(function () {

    $(".mobile-hamburger-wrapper").on("click", function () {

        $(this).hide();
        $(".mobile-menu-cross-wrapper").show();
        $(".mobile-menu-wrapper").slideDown();


    });
    $(".mobile-menu-cross-wrapper").on("click", function () {

        $(this).hide();
        $(".mobile-hamburger-wrapper").show();
        $(".mobile-menu-wrapper").slideUp();



    });
    $(".read-more-text").on("click", function () {

        $(this).hide();
        $(this).closest(".text-box").css({"overflow":"auto","max-height":"100%"});
       

    });
});
